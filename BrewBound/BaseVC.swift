//
//  BaseVC.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class BaseVC: UIViewController, RestDelegate {
    var delegate : RestDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self;

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadData(httpRequest: HttpObject){
        let downloader = AlamofireManager(httpRequest: httpRequest, restDelegate: self);
        downloader.startDownload();
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Mark: RestDelegate
    func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES) {
        print("OnPreExecute");
    }
    
    func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        print("onSuccess");
        return true;
    }
    
    func onFailure(_ paramObject: HttpResponse, forTaskCode taskCode: TASKCODES) {
        print("onFailure");
    }

}
