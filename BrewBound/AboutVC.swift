//
//  AboutVC.swift
//  BrewBound
//
//  Created by Nitin Bansal on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {

    @IBOutlet weak var serialNoLabel: UILabel!
    @IBAction func onMOtpClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        let vc = VCUtil.getViewController(identifier: "ViewController");
        self.present(vc, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let sNo = UserDefaults.standard.string(forKey: "SerialNo");
        serialNoLabel?.text = "Serial No: #\(sNo!)"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
