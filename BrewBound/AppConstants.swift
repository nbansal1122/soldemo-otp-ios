//
//  AppConstants.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

enum METHODS : Int {
    case GET
    case POST
    case PUT
    case DELETE
}
enum TASKCODES : Int{
    case GET_DATA
    case GET_TOKEN_RESPONSE
}
enum HttpResponseResult : Int{
    case SUCCESS
    case FAILURE
}

enum COLORS : String{
    case PRIMARY = "#534672"
    case PRIMARY_DARK = "#345678"
    case TITLE_COLOR = "#646464"
    case SUBTITLE_COLOR = "#465783"
    
}

struct URL {
    static let TEST_GET_URL = "https://raw.githubusercontent.com/tristanhimmelman/AlamofireObjectMapper/d8bb95982be8a11a2308e779bb9a9707ebe42ede/sample_json";
    static let GET_TOKEN_RESPONSE = "http://otp.solbanking.com/api/OTPToken";
    
}

struct PARAMS {
    static let  charList: [Character] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    
    static let numberList : [String] = ["317", "529", "612", "733", "320", "245", "561", "863", "963", "518", "213", "417", "622", "825", "972", "711", "525", "792", "435", "248", "297", "475", "562", "640", "731", "786", "511", "352", "753", "354", "225", "546", "677", "798", "998", "713"];
    
    static let SECURITY_KEY = "SOLSoftwarePrivateLimited";
    static let SERIAL_NO = "0005017146";
    static let TOKEN = "5unvdSlNzmKSf79oq0wjK1i79gYWNoVr5EWMPGLvDSAS7gzoalHuueMc2+DmpX2yRok/jEveyMQ=";
    
    
}

protocol RestDelegate: NSObjectProtocol {
    func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool
    
    func onFailure(_ paramObject: HttpResponse, forTaskCode taskCode: TASKCODES)
    func onPreExecute(httpRequestObject: HttpObject, forTaskCode taskcode: TASKCODES)
}
