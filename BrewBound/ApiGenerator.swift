//
//  ApiGenerator.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class ApiGenerator: NSObject {

    static func fetchWeatherData()->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.GET;
        httpRequest.strUrl = URL.TEST_GET_URL;
        return httpRequest;
    }
    static func fetchTokenResponse()->HttpObject{
        let httpRequest : HttpObject = HttpObject();
        httpRequest.methodType = METHODS.POST;
        httpRequest.strUrl = URL.GET_TOKEN_RESPONSE;
        httpRequest.strtaskCode = TASKCODES.GET_TOKEN_RESPONSE;
        return httpRequest;
    }
}
