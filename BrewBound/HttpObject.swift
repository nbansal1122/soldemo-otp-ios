//
//  HttpObject.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
enum RtStatus : Int {
    case success
    case failed_UNKNOWN
    case failed_CONNECTION_ERROR
    case failed_SESSION_EXPIRED
    case failed_NO_INTERNET
    case failed_STATUS_FALSE
    case failed_PARSE_EXCEPTION
    case failed_NO_DATA_EXCEPTION
}
class HttpObject {
    var methodType : METHODS = METHODS.GET
    var strUrl: String = ""
    var strMethodType: String = "GET"
    var dicParams = [String:Any]()
    var dicHeaders = [String: String]()
    var strtaskCode: TASKCODES = TASKCODES.GET_DATA
    var dicJson : NSMutableDictionary?
    var isArray = false
    func setJson(dicJson : NSMutableDictionary){
        self.dicJson = dicJson;
    }
    
    func addParam(_ key: String, andValue value: Any) {
        self.dicParams.updateValue(value,forKey:key)
    }
    func setJsonContentType() {
        self.dicHeaders.updateValue("application/json",forKey:"Content-Type")
    }
    
    
    
    func addHeader(_ key: String, andValue value: String) {
        self.dicHeaders.updateValue(value,forKey:key)
    }
    func addAuthHeader() {
        
    }
    
    func setPostMethod(){
        self.strMethodType = "POST"
        self.methodType = METHODS.POST;
    }
    
    func setPutMethod(){
        self.strMethodType = "PUT"
        self.methodType = METHODS.PUT
    }
    
    init(){
        self.dicParams = [String:String]()
        self.dicHeaders = [String: String]()
    }
    
    init(url pageUrl: String, withTaskCode taskCode: TASKCODES) {
        
        self.dicParams = [String:String]()
        self.dicHeaders = [String: String]()
        self.strUrl = pageUrl
        self.strtaskCode = taskCode
        addAuthHeader()
        
    }
}
