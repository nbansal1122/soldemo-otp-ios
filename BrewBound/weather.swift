//
//  weather.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

import ObjectMapper

class WeatherResponse: BaseMapperModel {
    var location: String?
    var threeDayForecast: [Forecast]?
    
    required init?(map: Map){
        super.init(map: map);
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        location <- map["location"]
        threeDayForecast <- map["three_day_forecast"]
    }
}

class Forecast: BaseMapperModel {
    var day: String?
    var temperature: Int?
    var conditions: String?
    
    required init?(map: Map){
        super.init(map: map);
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        day <- map["day"]
        temperature <- map["temperature"]
        conditions <- map["conditions"]
    }
}
