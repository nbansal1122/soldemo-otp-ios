//
//  JsonParser.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
class JsonParser: NSObject {

    static func parseJson(taskCode:TASKCODES, response : DataResponse<String>)->BaseMapperModel{
        var model : BaseMapperModel?
        switch taskCode {
        default:
            return parseTokenResponse(response: response);
        }
        return model!;
    }
    
    
    static func parseTokenResponse(response : DataResponse<String>)->TokenResponse{
        let tokenResponse : TokenResponse = TokenResponse(JSONString: response.result.value!)!;
        return tokenResponse;
    }
}
