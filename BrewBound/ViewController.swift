//
//  ViewController.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import Security
import Foundation

class ViewController: BaseVC {
    var seconds = 60;
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var serialNoLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var otpLabel: UILabel!
    var serialNo = "Serial No"
    var timer = Timer();
    let decryptedResult = "DokuIV788GrEQvrCeqEUp9Fz3KHwPUG02NPK88qqOyPkjr2wBS";
    static let  charList: [Character] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    static let numberList : [String] = ["317", "529", "612", "733", "320", "245", "561", "863", "963", "518", "213", "417", "622", "825", "972", "711", "525", "792", "435", "248", "297", "475", "562", "640", "731", "786", "511", "352", "753", "354", "225", "546", "677", "798", "998", "713"];
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isTokenAvailable(){
            runTimer(timeInterval: 1);
        }else{
            fetchTokenResponse()
        }

    }
    
    func fetchWeatherData(){
        downloadData(httpRequest: ApiGenerator.fetchWeatherData());
    }
    
    func fetchTokenResponse(){
        downloadData(httpRequest: ApiGenerator.fetchTokenResponse());
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func onSuccess(_ object: HttpResponse, forTaskCode taskcode: TASKCODES, httpRequestObject httpRequest: HttpObject) -> Bool {
        super.onSuccess(object, forTaskCode: taskcode, httpRequestObject: httpRequest);
        switch taskcode {
        case TASKCODES.GET_TOKEN_RESPONSE:
            onTokenResponseReceived(object);
            break
        default:
            break;
        }
        return false;
    }
    
    func onTokenResponseReceived(_ object: HttpResponse){
        let tokenResponse : TokenResponse = object.responseObject as! TokenResponse;
        if tokenResponse.error == false{
            if let data = tokenResponse.data{
                if data.count>0{
                    saveData(serialNumber: data[0].serialNo!, tokenKey: data[0].tokenKey!);
                    runTimer(timeInterval: 1)
                }
            }
        }else{
            print("Invalid Token Response with Message:\(tokenResponse.message)")
        }
    }
    
    func saveData(serialNumber : String, tokenKey : String){
        UserDefaults.standard.set(serialNumber, forKey: "SerialNo")
        UserDefaults.standard.set(tokenKey, forKey: "TokenKey")
        UserDefaults.standard.set(true, forKey: "isTokenAvailable")
    }
    
    func isTokenAvailable()->Bool{
        let isTokenAvailable = UserDefaults.standard.bool(forKey: "isTokenAvailable");
        return isTokenAvailable;
    }
    
    func runTimer(timeInterval : Int) {
        setOTP()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ViewController.updateTimer)), userInfo: nil, repeats: true)
        timer.fire();
    }
    
    func updateTimer(){
        seconds = seconds-1;
        if seconds < 1{
            seconds = 60;
            setOTP()
            //            runTimer(timeInterval: 1);
        }
        setProgress(progress: seconds);
//        print("seconds:\(seconds)")
    }
    
    func setProgress(progress: Int){
//        print("progress\(progress)")
        dateLabel?.text = getDateString();
        passwordLabel?.text = "Password will renew in \(seconds) seconds";
    }
    
    func setOTP(){
        let date = NSDate()
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.second], from: date as Date)
        let sec = components.second! as Int
        seconds = 60-sec;
        let sNo = UserDefaults.standard.string(forKey: "SerialNo");
        let token = UserDefaults.standard.string(forKey: "TokenKey");
        let otp = getFormattedOTP(key: token!);
        otpLabel?.text = getDisplayOTP(str: otp);
        serialNoLabel?.text = "Serial No: #\(sNo!)"
    }
    
    func getDisplayOTP(str : String)->String{
        let index = str.index(str.startIndex, offsetBy: 3)
        let index2 = str.index(str.startIndex, offsetBy: 3)
        return "\(str.substring(to: index)) \(str.substring(from: index2))";
    }
    
    func getFormattedOTP(key : String)->String{
        
        var newKey = key.uppercased().replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ".", with: "");
        var iVal : CLong = 0;
        
        print("NewKey:\(newKey)")
        for i in 0..<newKey.characters.count{
            let index = ViewController.charList.index(of: newKey[i] as Character)
            let number = Int.init(ViewController.numberList[index!]);
            iVal = iVal + number!;
        }
        print("iVal:\(iVal)")
        let date = NSDate()
        var calendar = NSCalendar.current
        calendar.timeZone = TimeZone.init(abbreviation: "GMT")!;
        let components = calendar.dateComponents([.hour, .minute, .year], from: date as Date)
        
        let year = components.year! + 1
        let minute = components.minute! + 1
        let day = calendar.ordinality(of: .day, in: .year, for: date as Date)! as Int + 1;
        let hour = components.hour! + 1
        
        print("Year:\(year)")
        print("day:\(day)")
        print("hour:\(hour)")
        print("minute:\(minute)")
        
        
        
        var randNum = minute*hour*year*day;
        randNum = randNum % iVal;
        
        print("randNum:\(randNum)")
        
        if randNum<999{
            randNum = randNum * 999;
        }else if randNum < 9999{
            randNum = randNum * 99;
        }else if randNum < 99999{
            randNum = randNum * 9;
        }
        let returnValue = String.init(format: "%6d", randNum);
        print("retunrOTP \(returnValue)")
        return returnValue;
    }
    
    func getDateString()->String{
        let date = Date();
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "dd MMM yyyy HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "GMT")
        let timeStamp = dateFormatter.string(for: date as Date)
        print("Date Format: \(timeStamp)");
        return timeStamp!
    }
        
    @IBAction func onAboutClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        let vc = VCUtil.getViewController(identifier: "AboutVC");
        self.present(vc, animated: true, completion: nil)
    }
    
}

