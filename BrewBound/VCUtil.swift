//
//  VCUtil.swift
//  BrewBound
//
//  Created by Nitin Bansal on 29/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit

class VCUtil: NSObject {

    static func getStoryBoard(name : String)->UIStoryboard{
        return UIStoryboard(name: name, bundle: Bundle.main)
    }
    
    static func getViewController(storyBoard: UIStoryboard, identifier : String)->UIViewController{
        return storyBoard.instantiateViewController(withIdentifier: identifier)
    }
    
    static func getViewController(identifier : String)->UIViewController{
        return getStoryBoard(name: "Main").instantiateViewController(withIdentifier: identifier)
    }
}
