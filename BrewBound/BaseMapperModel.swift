//
//  BaseMapperModel.swift
//  BrewBound
//
//  Created by Nitin Bansal on 20/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import UIKit
import ObjectMapper
class BaseMapperModel: Mappable {
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
    }
}
