//
//  Decryption.swift
//  BrewBound
//
//  Created by Nitin Bansal on 24/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import Foundation
import Security

class Decryption{
//    static func base64Decoded(data : String) -> NSData {
//        let decodedData = NSData(base64EncodedString: data, options:NSData.Base64DecodingOptions.fromRaw(0)!)
//        return decodedData
//    }
    
    static func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
//    static func decryptToken(token : String){
//        
//        
//        let enc = try! token.aesEncrypt(token, iv: iv);
//        
////        let data : Data = MD5(AppConstants.PARAMS.);
//        
//    }
    
//    static func getMD5Digest()->Data{
//        let myKeyData : NSData = ("SOLSoftwarePrivateLimited" as NSString).data(using: String.Encoding.utf8.rawValue)! as NSData
//        let mykeydatamd5 = Data(bytes: myKeyData.md5().bytes, count: 16)
//        return mykeydatamd5;
//    }
    
    
    
//    func myEncrypt(encryptData:String) -> String?{
//        
//        
//        
//        let myKeyData : NSData = ("SOLSoftwarePrivateLimited" as NSString).data(using: String.Encoding.utf8.rawValue)! as NSData
//        let myRawData : NSData = encryptData.data(using: String.Encoding.utf8)! as NSData
//        
//        
////        let mykeydatamd5 = Data(bytes: myKeyData.bytes, count: 16) // this key convert to 24 bytes but does not hash to md5
//        
//        let mykeydatamd5 = Data(bytes: myKeyData.md5().bytes, count: 24) // this line converted key to md5(), 24byte, but it
//        
//        
//        let buffer_size : size_t = myRawData.length + kCCBlockSize3DES
//        let buffer = UnsafeMutablePointer<NSData>.allocate(capacity: buffer_size)
//        var num_bytes_encrypted : size_t = 0
//        
//        let operation: CCOperation = UInt32(kCCEncrypt)
//        let algoritm:  CCAlgorithm = UInt32(kCCAlgorithm3DES)
//        let options:   CCOptions   = UInt32(kCCOptionECBMode | kCCOptionPKCS7Padding)
//        let keyLength        = size_t(kCCKeySize3DES)
//        
//        let Crypto_status: CCCryptorStatus =  CCCrypt(operation, algoritm, options, mykeydatamd5.bytes  , keyLength, nil, myRawData.bytes, myRawData.count, buffer, buffer_size, &num_bytes_encrypted)
//        
//        if UInt32(Crypto_status) == UInt32(kCCSuccess){
//            
//            let myResult: NSData = NSData(bytes: buffer, length: num_bytes_encrypted)
//            
//            free(buffer)
//            
//            return myResult.base64EncodedString(options: [])
//        }else{
//            free(buffer)
//            
//            return nil
//        }
//    }
    
    //    byte[] toDecryptedArray = Base64.decode(cipherText,Base64.DEFAULT);
    //    MessageDigest md = MessageDigest.getInstance("MD5");
    //    md.update(URLEncoder.encode(securityKey,"UTF-8").getBytes());
    //    byte[] digest = md.digest();
    //    byte[] key;
    //    if (digest.length == 16) {
    //    key = new byte[24];
    //    System.arraycopy(digest, 0, key, 0, 16);
    //    System.arraycopy(digest, 0, key, 16, 8);
    //    } else {
    //    key = digest;
    //    }
    //    Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
    //    SecretKeySpec secretKeySpec = new SecretKeySpec(key, "DESede");
    //    cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
    //    byte[] result = cipher.doFinal(toDecryptedArray);
    //    String decryptedResult = new String(result,"UTF-8");
    //    return decryptedResult;
}
