//
//  Decrypt.swift
//  BrewBound
//
//  Created by Nitin Bansal on 24/05/17.
//  Copyright © 2017 Nitin Bansal. All rights reserved.
//

import Foundation

extension String{
//    func base64Encoded() -> String {
//        let plainData = data(usingEncoding: NSUTF8StringEncoding)
//        let base64String = plainData?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.fromRaw(0)!)
//        return base64String!
//    }
    
//    func base64Decoded() -> String {
//        let decodedData = NSData(base64EncodedString: self, options:NSDataBase64DecodingOptions.fromRaw(0)!)
//        let decodedString = NSString(data: decodedData, encoding: NSUTF8StringEncoding)
//        return decodedString
//    }
    
    subscript (i: Int) -> Character {
        return self[self.characters.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return self[start..<end]
    }
    
    subscript (r: ClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return self[start...end]
    }
    
    
}
